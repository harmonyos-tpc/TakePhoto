package org.devio.takephoto.uitl;

import ohos.aafwk.ability.Ability;
import ohos.app.Context;
import ohos.app.Environment;
import ohos.media.image.ImagePacker;
import ohos.utils.net.Uri;
import org.devio.takephoto.ResourceTable;
import org.devio.takephoto.model.TException;
import org.devio.takephoto.model.TExceptionType;

import java.io.*;
import java.util.UUID;

/**
 * ImageFiles
 *
 * @author JPH
 *         Date 2016/6/7 0007 9:39
 */
public class TImageFiles {
    private static final String TAG = TImageFiles.class.getName();

    public static void writeToFile(ImagePacker bitmap, Uri imageUri) {
        if (bitmap == null) {
            return;
        }
        File file = new File(imageUri.getDecodedPath());
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ImagePacker.PackingOptions packingOptions = new ImagePacker.PackingOptions();
        packingOptions.format = "image/jpeg";
        bitmap.initializePacking(bos.toByteArray(), 100, packingOptions);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            fos.write(bos.toByteArray());
            bos.flush();
            fos.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                if (bos != null) {
                    bos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void inputStreamToFile(InputStream is, File file) throws TException {
        if (file == null) {
            throw new TException(TExceptionType.TYPE_WRITE_FAIL);
        }
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            byte[] buffer = new byte[1024 * 10];
            int i;
            while ((i = is.read(buffer)) != -1) {
                fos.write(buffer, 0, i);
            }
        } catch (IOException e) {
            throw new TException(TExceptionType.TYPE_WRITE_FAIL);
        } finally {
            try {
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static File getTempFile(Ability context, Uri photoUri) throws TException {
        String mimeType = getMimeType(context, photoUri);
        if (!checkMimeType(context, mimeType)) {
            throw new TException(TExceptionType.TYPE_NOT_IMAGE);
        }
        File filesDir = context.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        if (!filesDir.exists()) {
            filesDir.mkdirs();
        }
        File photoFile = new File(filesDir, UUID.randomUUID().toString() + "." + mimeType);
        return photoFile;
    }

    public static boolean checkMimeType(Context context, String minType) {
        boolean isPicture = !TextUtils.isEmpty(minType) && (".jpg|.gif|.png|.bmp|.jpeg|.webp|".contains(minType.toLowerCase()));
        if (!isPicture) {
            UiUtil.showToast(context, ResUtil.getString(context, ResourceTable.String_tip_type_not_image));
        }
        return isPicture;
    }

    public static String getMimeType(Ability context, Uri uri) {
        File file = TUriParse.getFileWithUri(uri, context);
        if (file != null) {
            String fileName = file.getName();
            return getMimeTypeByFileName(fileName);
        }
        return null;
    }

    public static String getMimeTypeByFileName(String fileName) {
        return fileName.substring(fileName.lastIndexOf("."), fileName.length());
    }
}
