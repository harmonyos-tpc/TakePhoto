package org.devio.takephoto.uitl;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.window.dialog.CommonDialog;
import ohos.app.Context;
import ohos.rpc.RemoteException;
import ohos.utils.net.Uri;
import org.devio.takephoto.ResourceTable;
import org.devio.takephoto.model.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Author: JPH
 * Date: 2016/7/26 10:04
 */
public class TUtils {
    private static final String TAG = TUtils.class.getName();

    public static ArrayList<Uri> convertImageToUri(Context context, ArrayList<Image> images) throws TException {
        ArrayList<Uri> uris = new ArrayList<>();
        for (Image image : images) {
            uris.add(Uri.getUriFromFile(new File(image.path)));
        }
        return uris;
    }

    public static ArrayList<TImage> getTImagesWithImages(ArrayList<Image> images, TImage.FromType fromType) {
        ArrayList<TImage> tImages = new ArrayList<>();
        for (Image image : images) {
            tImages.add(TImage.of(image.path, fromType));
        }
        return tImages;
    }

    public static ArrayList<TImage> getTImagesWithUris(ArrayList<Uri> uris, TImage.FromType fromType) {
        ArrayList<TImage> tImages = new ArrayList<>();
        for (Uri uri : uris) {
            tImages.add(TImage.of(uri, fromType));
        }
        return tImages;
    }

    public static void startActivityForResult(TContextWrap contextWrap, TIntentWap intentWap) {
        if (contextWrap.getFraction() != null) {
            contextWrap.getFraction().startAbility(intentWap.getIntent(), intentWap.getRequestCode());
        } else {
            contextWrap.getAbility().startAbilityForResult(intentWap.getIntent(), intentWap.getRequestCode());
        }
    }

    public static void sendIntentBySafely(TContextWrap contextWrap, List<TIntentWap> intentWapList, int defaultIndex, boolean isCrop)
            throws TException {
        if (defaultIndex + 1 > intentWapList.size()) {
            throw new TException(isCrop ? TExceptionType.TYPE_NO_MATCH_PICK_INTENT : TExceptionType.TYPE_NO_MATCH_CROP_INTENT);
        }
        TIntentWap intentWap = intentWapList.get(defaultIndex);
        List result = null;
        try {
            result = contextWrap.getAbility().getBundleManager().queryAbilityByIntent(intentWap.getIntent(), 1, 0);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        if (result.isEmpty()) {
            sendIntentBySafely(contextWrap, intentWapList, ++defaultIndex, isCrop);
        } else {
            startActivityForResult(contextWrap, intentWap);
        }
    }

    public static void captureBySafely(TContextWrap contextWrap, TIntentWap intentWap) throws TException {
        List result = null;
        try {
            result = contextWrap.getAbility().getBundleManager().queryAbilityByIntent(intentWap.getIntent(), 1, 0);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        if (result.isEmpty()) {
            UiUtil.showToast(contextWrap.getAbility(), ResUtil.getString(contextWrap.getAbility(), ResourceTable.String_tip_no_camera));
            throw new TException(TExceptionType.TYPE_NO_CAMERA);
        } else {
            startActivityForResult(contextWrap, intentWap);
        }
    }

    public static void cropWithOtherAppBySafely(TContextWrap contextWrap, Uri imageUri, Uri outPutUri, CropOptions options) {
        Intent nativeCropIntent = IntentUtils.getCropIntentWithOtherApp(imageUri, outPutUri, options);
        List result = null;
        try {
            result = contextWrap.getAbility().getBundleManager().queryAbilityByIntent(nativeCropIntent, 1, 0);
        } catch (RemoteException e) {
            e.printStackTrace();
        }
        if (result.isEmpty()) {
            cropWithOwnApp(contextWrap, imageUri, outPutUri, options);
        } else {
            startActivityForResult(contextWrap,
                    new TIntentWap(IntentUtils.getCropIntentWithOtherApp(imageUri, outPutUri, options), TConstant.RC_CROP));
        }
    }

    public static void cropWithOwnApp(TContextWrap contextWrap, Uri imageUri, Uri outPutUri, CropOptions options) {
        if (options.getAspectX() * options.getAspectY() > 0) {
            // TODO
        }
    }

    public static CommonDialog showProgressDialog(final Ability activity, String... progressTitle) {
        if (activity == null || activity.isTerminating()) {
            return null;
        }
        String title = ResUtil.getString(activity, ResourceTable.String_tip_tips);
        if (progressTitle != null && progressTitle.length > 0) {
            title = progressTitle[0];
        }
        CommonDialog progressDialog = new CommonDialog(activity);
        progressDialog.setTitleText(title);
        progressDialog.show();
        return progressDialog;
    }
}
