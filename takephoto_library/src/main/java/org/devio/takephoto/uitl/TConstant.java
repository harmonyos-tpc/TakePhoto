package org.devio.takephoto.uitl;

import ohos.app.Context;
/**
 * @author JPH
 *         Date 2016/6/7 0007 9:39
 */
public class TConstant {
    public final static int RC_CROP = 1001;
    public final static int RC_PICK_PICTURE_FROM_CAPTURE_CROP = 1002;
    public final static int RC_PICK_PICTURE_FROM_CAPTURE = 1003;
    public final static int RC_PICK_PICTURE_FROM_DOCUMENTS_ORIGINAL = 1004;
    public final static int RC_PICK_PICTURE_FROM_DOCUMENTS_CROP = 1005;
    public final static int RC_PICK_PICTURE_FROM_GALLERY_ORIGINAL = 1006;
    public final static int RC_PICK_PICTURE_FROM_GALLERY_CROP = 1007;
    public final static int RC_PICK_MULTIPLE = 1008;
    public final static int PERMISSION_REQUEST_TAKE_PHOTO = 2000;
    public static final int CAMERA_SUCCESS = 1021;
    public static final float FLOAT_HALF = 0.5f;

    public final static String getFileProviderName(Context context) {
        return context.getBundleName() + ".fileprovider";
    }
    public static final String APP_BUNDLE_NAME = "org.devio.simple";
    public static final String CAMERA_ABILITY_NAME = "org.devio.simple.CameraAbility";
    public static final String URI_TYPE = "image/*";
    public static final String REQUEST_FILE_PATH = "camera_file";
    public static final String IMG_FILE_TYPE = ".jpg";
    // PACMAP bundle constants
    public static final String PC_IMAGES = "images";
    public static final String PC_CROP = "crop";
    public static final String PC_TRUE = "true";
    public static final String PC_ASPECT_X = "aspectX";
    public static final String PC_ASPECT_Y = "aspectY";
    public static final String PC_OUTPUT_X = "outputX";
    public static final String PC_OUTPUT_Y = "outputY";
    public static final String PC_SCALE = "scale";
    public static final String PC_OUTPUT_FORMAT = "outputFormat";
    public static final String PC_IMAGE_JPEG = "image/jpeg";
    public static final String PC_NO_FACE_DETECTION = "noFaceDetection";
    public static final String PC_ORIENTATION = "Orientation";
    public static final String PC_CROP_OPTIONS = "cropOptions";
    public static final String PC_TAKE_PHOTO_OPTIONS = "takePhotoOptions";
    public static final String PC_SHOW_COMPRESSION_DIALOG = "showCompressDialog";
    public static final String PC_OUTPUT_URI = "outPutUri";
    public static final String PC_TEMP_URI = "tempUri";
    public static final String PC_COMPRESS_CONFIG = "compressConfig";
    public static final String DEFAULT_DISK_CACHE_DIR = "takephoto_cache";
}