package org.devio.takephoto.uitl;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.media.image.ImagePacker;
import ohos.utils.net.Uri;
import org.devio.takephoto.model.CropOptions;

/**
 * Intent
 * Author: JPH
 * Date: 2016/6/7 0007 13:41
 */
public class IntentUtils {
    public static Intent getPickMultipleIntent() {
        return new Intent();
    }

    static Intent getCropIntentWithOtherApp(Uri targetUri, Uri outPutUri, CropOptions options) {
        Intent intent = new Intent();
        intent.setUriAndType(targetUri, TConstant.URI_TYPE);
        intent.setParam(TConstant.PC_CROP, TConstant.PC_TRUE);
        if (options.getAspectX() * options.getAspectY() > 0) {
            intent.setParam(TConstant.PC_ASPECT_X, options.getAspectX());
            intent.setParam(TConstant.PC_ASPECT_Y, options.getAspectY());
        }
        if (options.getOutputX() * options.getOutputY() > 0) {
            intent.setParam(TConstant.PC_OUTPUT_X, options.getOutputX());
            intent.setParam(TConstant.PC_OUTPUT_Y, options.getOutputY());
        }
        intent.setParam(TConstant.PC_SCALE, true);
        intent.setParam(TConstant.PC_IMAGES, outPutUri);
        intent.setParam(TConstant.PC_OUTPUT_FORMAT, new ImagePacker.PackingOptions().format);
        intent.setParam(TConstant.PC_NO_FACE_DETECTION, true);
        return intent;
    }

    public static Intent getCaptureIntent(Uri outPutUri) {
        Operation operationBuilder = new Intent.OperationBuilder()
                .withBundleName(TConstant.APP_BUNDLE_NAME)
                .withAbilityName(TConstant.CAMERA_ABILITY_NAME)
                .build();
        Intent intent = new Intent();
        intent.setOperation(operationBuilder);
        intent.setParam(TConstant.PC_IMAGES, outPutUri);
        return intent;
    }

    public static Intent getPickIntentWithGallery() {
        Intent intent = new Intent();
        intent.setType(TConstant.URI_TYPE);
        return intent;
    }

    public static Intent getPickIntentWithDocuments() {
        Intent intent = new Intent();
        intent.setType(TConstant.URI_TYPE);
        return intent;
    }
}
