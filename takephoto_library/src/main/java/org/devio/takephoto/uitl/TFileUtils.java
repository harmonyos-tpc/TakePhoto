package org.devio.takephoto.uitl;

import ohos.app.Context;

import java.io.File;

/**
 * Author: crazycodeboy
 * Date: 2016/11/5 0007 20:10
 * Version:4.0.0
 * http://www.devio.org/
 * GitHub:https://github.com/crazycodeboy
 * Email:crazycodeboy@gmail.com
 */
public class TFileUtils {
    private static final String TAG = TFileUtils.class.getSimpleName();

    /**
     * This method is used to get cache directory
     *
     * @param context context of your ability/fraction
     * @param file    file taken by camera
     * @return File with cache directory
     */
    public static File getPhotoCacheDir(Context context, File file) {
        File cacheDir = context.getCacheDir();
        if (cacheDir != null) {
            File mCacheDir = new File(cacheDir, TConstant.DEFAULT_DISK_CACHE_DIR);
            if (!mCacheDir.mkdirs() && (!mCacheDir.exists() || !mCacheDir.isDirectory())) {
                return file;
            } else {
                return new File(mCacheDir, file.getName());
            }
        }
        return file;
    }

    /**
     * This method is used to delete file
     *
     * @param path path of file to be deleted
     */
    public static void delete(String path) {
        try {
            if (path == null) {
                return;
            }
            File file = new File(path);
            if (!file.delete()) {
                file.deleteOnExit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
