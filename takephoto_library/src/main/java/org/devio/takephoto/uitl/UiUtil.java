/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.devio.takephoto.uitl;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;

public class UiUtil {
    private Context mContext;

    public UiUtil(Context context) {
        mContext = context;
    }

    public static ShapeElement getLineShape(Context context, int width, int color) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(ShapeElement.LINE);
        shapeElement.setStroke(width, RgbColor.fromArgbInt(ResUtil.getColor(context, color)));
        return shapeElement;
    }

    public final ShapeElement getShapeElement(int shape, int color, float radius) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setShape(shape);
        shapeElement.setRgbColor(RgbColor.fromArgbInt(ResUtil.getColor(mContext, color)));
        shapeElement.setCornerRadius(radius);
        return shapeElement;
    }

    public static void showToast(Context context, String message) {
        ToastDialog toastDialog = new ToastDialog(context);
        toastDialog.setText(message);
        toastDialog.setDuration(3);
        if (toastDialog.isShowing()) {
            toastDialog.show();
        }
    }
}
