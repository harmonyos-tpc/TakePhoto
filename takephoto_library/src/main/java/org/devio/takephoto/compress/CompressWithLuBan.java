/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.devio.takephoto.compress;

import ohos.app.Context;
import org.devio.takephoto.ResourceTable;
import org.devio.takephoto.compress.CompressImage;
import org.devio.takephoto.model.TImage;
import org.devio.takephoto.uitl.ResUtil;
import top.zibin.luban.Luban;
import top.zibin.luban.OnCompressListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class CompressWithLuBan implements CompressImage {
    private ArrayList<TImage> images;
    private CompressListener listener;
    private Context context;
    private ArrayList<File> files = new ArrayList<>();
    private static final int SIZE = 100;

    public CompressWithLuBan(Context context, ArrayList<TImage> images, CompressListener listener) {
        this.images = images;
        this.listener = listener;
        this.context = context;
    }

    @Override
    public void compress() {
        if (images == null || images.isEmpty()) {
            listener.onCompressFailed(images, ResUtil.getString(context, ResourceTable.String_image_null));
            return;
        }
        for (TImage image : images) {
            if (image == null) {
                listener.onCompressFailed(images, ResUtil.getString(context, ResourceTable.String_picture_compress_null));
                return;
            }
            files.add(new File(image.getOriginalPath()));
        }
        for (File file : files) {
            try {
                compressFile(file);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void compressFile(File fileObj) throws IOException {
        Luban.with(context)
                .setTargetDir(fileObj.getCanonicalPath())
                .ignoreBy(SIZE)
                .load(files)
                .setCompressListener(new OnCompressListener() {
                    @Override
                    public void onStart() {
                    }

                    @Override
                    public void onSuccess(File file) {
                        handleCompressCallBack(file);
                    }

                    @Override
                    public void onError(Throwable e) {
                        listener.onCompressFailed(images, e.getMessage() + ResUtil.getString(context, ResourceTable.String_compress_failure));
                    }
                })
                .launch();
    }

    private void handleCompressCallBack(File file) {
        TImage image = images.get(0);
        image.setCompressed(true);
        image.setCompressPath(file.getPath());
        listener.onCompressSuccess(images);
    }
}
