package org.devio.takephoto.compress;

import org.devio.takephoto.model.TImage;

import java.util.ArrayList;

/**
 * <p>
 * Author JPH
 * Date 2015-08-26 1:44:26
 */
public interface CompressImage {
    void compress();

    interface CompressListener {
        void onCompressSuccess(ArrayList<TImage> images);

        void onCompressFailed(ArrayList<TImage> images, String msg);
    }
}
