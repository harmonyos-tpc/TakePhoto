package org.devio.takephoto.compress;

import ohos.app.Context;
import org.devio.takephoto.ResourceTable;
import org.devio.takephoto.model.TImage;
import org.devio.takephoto.uitl.ResUtil;
import org.devio.takephoto.uitl.TextUtils;

import java.io.File;
import java.util.ArrayList;

/**
 * <p>
 * Date: 2016/9/21 0007 20:10
 * Version:3.0.0
 * http://www.devio.org
 * GitHub:https://github.com/crazycodeboy
 * Email:crazycodeboy@gmail.com
 */
public class CompressImageImpl implements CompressImage {
    private static final String TAG = CompressImageImpl.class.getSimpleName();
    private CompressImageUtil compressImageUtil;
    private ArrayList<TImage> images;
    private CompressImage.CompressListener listener;
    private Context mContext;

    public static CompressImage of(Context context, CompressConfig config, ArrayList<TImage> images,
                                   CompressImage.CompressListener listener) {
        if (config.getLubanOptions() != null) {
            return new CompressWithLuBan(context, images, listener);
        } else {
            return new CompressImageImpl(context, config, images, listener);
        }
    }

    private CompressImageImpl(Context context, CompressConfig config, ArrayList<TImage> images, CompressImage.CompressListener listener) {
        mContext = context;
        compressImageUtil = new CompressImageUtil(context, config);
        this.images = images;
        this.listener = listener;
    }

    @Override
    public void compress() {
        if (images == null || images.isEmpty()) {
            listener.onCompressFailed(images, ResUtil.getString(mContext, ResourceTable.String_image_null));
        }
        for (TImage image : images) {
            if (image == null) {
                listener.onCompressFailed(images, ResUtil.getString(mContext, ResourceTable.String_picture_compress_null));
                return;
            }
        }
        compress(images.get(0));
    }

    private void compress(final TImage image) {
        if (TextUtils.isEmpty(image.getOriginalPath())) {
            continueCompress(image, false);
            return;
        }

        File file = new File(image.getOriginalPath());
        if (!file.exists() || !file.isFile()) {
            continueCompress(image, false);
            return;
        }

        compressImageUtil.compress(image.getOriginalPath(), new CompressImageUtil.CompressListener() {
            @Override
            public void onCompressSuccess(String imgPath) {
                image.setCompressPath(imgPath);
                continueCompress(image, true);
            }

            @Override
            public void onCompressFailed(String imgPath, String msg) {
                continueCompress(image, false, msg);
            }
        });
    }

    private void continueCompress(TImage image, boolean preSuccess, String... message) {
        image.setCompressed(preSuccess);
        int index = images.indexOf(image);
        boolean isLast = index == images.size() - 1;
        if (isLast) {
            handleCompressCallBack(message);
        } else {
            compress(images.get(index + 1));
        }
    }

    private void handleCompressCallBack(String... message) {
        if (message.length > 0) {
            listener.onCompressFailed(images, message[0]);
            return;
        }

        for (TImage image : images) {
            if (!image.isCompressed()) {
                listener.onCompressFailed(images, image.getCompressPath() + ResUtil.getString(mContext, ResourceTable.String_compress_failure));
                return;
            }
        }
        listener.onCompressSuccess(images);
    }
}
