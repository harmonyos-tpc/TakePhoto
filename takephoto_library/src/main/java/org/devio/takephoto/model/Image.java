/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.devio.takephoto.model;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;

public class Image implements Sequenceable {
    public long id;
    public String name;
    public String path;
    public boolean isSelected;

    public Image(long id, String name, String path, boolean isSelected) {
        this.id = id;
        this.name = name;
        this.path = path;
        this.isSelected = isSelected;
    }


    @Override
    public boolean hasFileDescriptor() {
        return false;
    }

    @Override
    public boolean marshalling(Parcel dest) {
        dest.writeLong(id);
        dest.writeString(name);
        dest.writeString(path);
        return false;
    }

    public static final Producer<Image> CREATOR = new Producer<Image>() {
        @Override
        public Image createFromParcel(Parcel source) {
            return new Image(source);
        }
    };

    private Image(Parcel in) {
        unmarshalling(in);
    }

    @Override
    public boolean unmarshalling(Parcel in) {
        id = in.readLong();
        name = in.readString();
        path = in.readString();
        return false;
    }
}
