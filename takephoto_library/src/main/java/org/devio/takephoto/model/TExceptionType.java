package org.devio.takephoto.model;

/**
 * Author: JPH
 * Date: 2016/7/26 11:01
 */
public enum TExceptionType {
    TYPE_NOT_IMAGE("The selected file is not an image."), TYPE_WRITE_FAIL("Failed to save the selected file."), TYPE_URI_NULL("The Uri of the selected photo is null."), TYPE_URI_PARSE_FAIL("Failed to obtain the file path from the URI."),
    TYPE_NO_MATCH_PICK_INTENT("No Intent of the selected image is found."), TYPE_NO_MATCH_CROP_INTENT("No Intent of the cropped image is found."), TYPE_NO_CAMERA("No camera."),
    TYPE_NO_FIND("The selected file is not found.");

    String stringValue;

    TExceptionType(String stringValue) {
        this.stringValue = stringValue;
    }

    public String getStringValue() {
        return stringValue;
    }
}
