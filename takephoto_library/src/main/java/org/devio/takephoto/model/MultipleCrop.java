package org.devio.takephoto.model;

import ohos.aafwk.ability.Ability;
import ohos.utils.net.Uri;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Author: JPH
 * Date: 2016/8/11 17:01
 */
public class MultipleCrop {
    private ArrayList<Uri> uris;
    private ArrayList<Uri> outUris;
    private ArrayList<TImage> tImages;
    private TImage.FromType fromType;
    public boolean hasFailed;

    public static MultipleCrop of(ArrayList<Uri> uris, Ability ability, TImage.FromType fromType) throws TException {
        return new MultipleCrop(uris, ability, fromType);
    }

    public static MultipleCrop of(ArrayList<Uri> uris, ArrayList<Uri> outUris, TImage.FromType fromType) {
        return new MultipleCrop(uris, outUris, fromType);
    }

    private MultipleCrop(ArrayList<Uri> uris, Ability ability, TImage.FromType fromType) throws TException {
        this.uris = uris;
        ArrayList<Uri> outUris = new ArrayList<>();
        this.outUris = outUris;
        this.fromType = fromType;
    }

    private MultipleCrop(ArrayList<Uri> uris, ArrayList<Uri> outUris, TImage.FromType fromType) {
        this.uris = uris;
        this.outUris = outUris;
        this.fromType = fromType;
    }

    public ArrayList<Uri> getUris() {
        return uris;
    }

    public void setUris(ArrayList<Uri> uris) {
        this.uris = uris;
    }

    public ArrayList<Uri> getOutUris() {
        return outUris;
    }

    public void setOutUris(ArrayList<Uri> outUris) {
        this.outUris = outUris;
    }

    public ArrayList<TImage> gettImages() {
        return tImages;
    }

    public void settImages(ArrayList<TImage> tImages) {
        this.tImages = tImages;
    }

    /**
     * Sets the cropped flag for the cropped picture.
     *
     * @param uri Cropped picture
     * @return Indicates whether the picture is the last one
     * @param cropped
     */
    public Map setCropWithUri(Uri uri, boolean cropped) {
        if (!cropped) {
            hasFailed = true;
        }
        int index = outUris.indexOf(uri);
        tImages.get(index).setCropped(cropped);
        Map result = new HashMap();
        result.put("index", index);
        result.put("isLast", index == outUris.size() - 1 ? true : false);
        return result;
    }
}
