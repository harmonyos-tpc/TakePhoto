package org.devio.takephoto.model;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.fraction.Fraction;

/**
 * Author: JPH
 * Date: 2016/8/11 17:01
 */
public class TContextWrap {
    private Ability ability;
    private Fraction fraction;

    public static TContextWrap of(Ability ability) {
        return new TContextWrap(ability);
    }

    public static TContextWrap of(Fraction fraction) {
        return new TContextWrap(fraction);
    }

    private TContextWrap(Ability ability) {
        this.ability = ability;
    }

    private TContextWrap(Fraction fraction) {
        this.fraction = fraction;
        this.ability = fraction.getFractionAbility();
    }

    public Ability getAbility() {
        return ability;
    }

    public void setAbility(Ability ability) {
        this.ability = ability;
    }

    public Fraction getFraction() {
        return fraction;
    }

    public void setFraction(Fraction fraction) {
        this.fraction = fraction;
    }
}
