package org.devio.takephoto.app;

import ohos.aafwk.content.Intent;
import ohos.utils.PacMap;
import ohos.utils.net.Uri;
import org.devio.takephoto.compress.CompressConfig;
import org.devio.takephoto.model.*;
import org.devio.takephoto.permission.PermissionManager;

/**
 * Author: crazycodeboy
 * Date: 2016/9/21 0007 20:10
 * Version:4.0.0
 * http://www.devio.org
 * GitHub:https://github.com/crazycodeboy
 * Email:crazycodeboy@gmail.com
 */
public interface TakePhoto {

    void onPickMultiple(int limit);

    void onPickMultipleWithCrop(int limit, CropOptions options);

    void onPickFromDocuments();

    void onPickFromDocumentsWithCrop(Uri outPutUri, CropOptions options);

    void onPickFromGallery();

    void onPickFromGalleryWithCrop(Uri outPutUri, CropOptions options);

    void onPickFromCapture(Uri outPutUri);

    void onPickFromCaptureWithCrop(Uri outPutUri, CropOptions options);

    void setTakePhotoOptions(TakePhotoOptions options);

    void onEnableCompress(CompressConfig config, boolean showCompressDialog);

    void permissionNotify(PermissionManager.TPermissionType type);

    void onCreate(Intent savedInstanceState);

    void onSaveInstanceState(PacMap outState);

    void onActivityResult(int requestCode, int resultCode, Intent data);

    void onCrop(Uri imageUri, Uri outPutUri, CropOptions options) throws TException;

    void onCrop(MultipleCrop multipleCrop, CropOptions options) throws TException;

    interface TakeResultListener {
        void takeSuccess(TResult result);

        void takeFail(TResult result, String msg);

        void takeCancel();
    }
}
