package org.devio.takephoto.permission;

import org.devio.takephoto.model.InvokeParam;

public interface InvokeListener {
    PermissionManager.TPermissionType invoke(InvokeParam invokeParam);
}
