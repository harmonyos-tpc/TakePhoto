/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.devio.simple;

import com.bumptech.glide.Glide;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import org.devio.takephoto.ResourceTable;
import org.devio.takephoto.model.TImage;
import org.devio.takephoto.uitl.TConstant;
import org.devio.takephoto.uitl.TextUtils;

import java.io.File;
import java.util.ArrayList;

public class ResultAbility extends Ability {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer componentContainer = (ComponentContainer) LayoutScatter.getInstance(this).parse(ResourceTable.Layout_ability_result_layout, null, false);
        setUIContent(componentContainer);
        Image imageView = (Image) componentContainer.findComponentById(ResourceTable.Id_iv_clickedImage);
        ArrayList<TImage> images = intent.getSerializableParam(TConstant.PC_IMAGES);
        if (images != null && images.size() > 0) {
            TImage tImage = images.get(0);
            if (!TextUtils.isEmpty(tImage.getCompressPath())) {
                Glide.with(this).load(new File(images.get(0).getCompressPath())).into(imageView);
            } else {
                Glide.with(this).load(new File(images.get(0).getOriginalPath())).into(imageView);
            }
        }
    }

    @Override
    protected void onBackPressed() {
        super.onBackPressed();
        terminateAbility();
    }
}