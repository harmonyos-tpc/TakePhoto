/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.devio.simple;

import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.ability.fraction.FractionManager;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.utils.PacMap;
import org.devio.takephoto.ResourceTable;
import org.devio.takephoto.model.TImage;
import org.devio.takephoto.uitl.TConstant;

import java.util.ArrayList;

public class SimpleFractionAbility extends FractionAbility {
    private SimpleFraction simpleFraction = new SimpleFraction();
    private boolean isFractionAdded = false;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_simple_fraction_layout);
        addFraction();
    }

    @Override
    protected void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    /**
     * This method is used to add fraction using fraction manager
     */
    private void addFraction() {
        FractionManager fractionManager = getFractionManager();
        if (isFractionAdded) {
            fractionManager.startFractionScheduler().remove(simpleFraction).submit();
        }
        fractionManager.startFractionScheduler()
                .add(ResourceTable.Id_fragment1, simpleFraction, SimpleFraction.class.getName())
                .submit();
        isFractionAdded = true;
    }

    @Override
    public void onSaveAbilityState(PacMap outState) {
        simpleFraction.onSaveInstanceState(outState);
        super.onSaveAbilityState(outState);
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        simpleFraction.onActivityResult(requestCode, resultCode, resultData);
        super.onAbilityResult(requestCode, resultCode, resultData);
    }

    @Override
    public void onRequestPermissionsFromUserResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsFromUserResult(requestCode, permissions, grantResults);
        simpleFraction.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    /**
     * This method is used to show image taken by camera
     *
     * @param images result images taken by camera
     */
    public void showImg(ArrayList<TImage> images) {
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(TConstant.APP_BUNDLE_NAME)
                .withAbilityName(ResultAbility.class.getSimpleName())
                .build();
        Intent intent = new Intent();
        intent.setParam(TConstant.PC_IMAGES, images);
        intent.setOperation(operation);
        startAbility(intent);
        terminateAbility();
    }
}
