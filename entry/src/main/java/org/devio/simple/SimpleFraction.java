/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.devio.simple;

import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.utils.PacMap;
import org.devio.takephoto.ResourceTable;
import org.devio.takephoto.app.TakePhotoFraction;
import org.devio.takephoto.model.TResult;
import org.devio.takephoto.permission.PermissionManager;

public class SimpleFraction extends TakePhotoFraction {
    private CustomHelper customHelper;

    @Override
    protected void onActive() {
        super.onActive();
    }

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component component = scatter.parse(ResourceTable.Layout_common_layout, container, false);
        customHelper = CustomHelper.of(component, getFractionAbility());
        initLayoutWidget(component);
        return component;
    }

    /**
     * This method is used to initialize layout components
     *
     * @param component root component for getting child view components
     */
    private void initLayoutWidget(Component component) {
        Button btnTakePhoto = (Button) component.findComponentById(ResourceTable.Id_btnPickByTake);
        btnTakePhoto.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                onClickButton(component);
            }
        });
        Button btnSelectPhoto = (Button) component.findComponentById(ResourceTable.Id_btnPickBySelect);
        btnSelectPhoto.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                onClickButton(component);
            }
        });
    }

    private void onClickButton(Component view) {
        customHelper.onClick(view, getTakePhoto());
    }

    void onSaveInstanceState(PacMap outState) {
        getTakePhoto().onSaveInstanceState(outState);
    }

    void onActivityResult(int requestCode, int resultCode, Intent data) {
        getTakePhoto().onActivityResult(requestCode, resultCode, data);
    }

    void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        PermissionManager.TPermissionType type = PermissionManager.onRequestPermissionsResult(requestCode, permissions, grantResults);
        PermissionManager.handlePermissionsResult(getFractionAbility(), type, invokeParam, this);
    }

    @Override
    public void takeCancel() {
        super.takeCancel();
    }

    @Override
    public void takeFail(TResult result, String msg) {
        super.takeFail(result, msg);
    }

    @Override
    public void takeSuccess(TResult result) {
        super.takeSuccess(result);
        if (getFractionAbility() instanceof SimpleFractionAbility) {
            ((SimpleFractionAbility) getFractionAbility()).showImg(result.getImages());
        }
    }
}
