/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.devio.simple;

import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import org.devio.simple.slice.SimpleAbilitySlice;
import org.devio.takephoto.app.TakePhotoAbility;
import org.devio.takephoto.model.TImage;
import org.devio.takephoto.model.TResult;
import org.devio.takephoto.uitl.TConstant;

import java.util.ArrayList;

public class SimpleAbility extends TakePhotoAbility {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setMainRoute(SimpleAbilitySlice.class.getName());
    }

    @Override
    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
        super.onAbilityResult(requestCode, resultCode, resultData);
    }

    @Override
    public void takeCancel() {
        super.takeCancel();
    }

    @Override
    public void takeFail(TResult result, String msg) {
        super.takeFail(result, msg);
    }

    @Override
    public void takeSuccess(TResult result) {
        super.takeSuccess(result);
        showImg(result.getImages());
    }

    /**
     * This method is used to show image taken by camera
     *
     * @param images resukt images taken by camera
     */
    private void showImg(ArrayList<TImage> images) {
        Operation operation = new Intent.OperationBuilder()
                .withBundleName(TConstant.APP_BUNDLE_NAME)
                .withAbilityName(ResultAbility.class.getSimpleName())
                .build();
        Intent intent = new Intent();
        intent.setParam(TConstant.PC_IMAGES, images);
        intent.setOperation(operation);
        startAbility(intent);
        terminateAbility();
    }
}
