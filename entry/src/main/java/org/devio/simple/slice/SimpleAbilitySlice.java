/**
 *
 */
package org.devio.simple.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import org.devio.simple.CustomHelper;
import org.devio.simple.SimpleAbility;
import org.devio.takephoto.ResourceTable;

public class SimpleAbilitySlice extends AbilitySlice {
    private CustomHelper customHelper;
    private SimpleAbility mSimpleAbility;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        mSimpleAbility = (SimpleAbility) getAbility();
        ComponentContainer rootView = (ComponentContainer) LayoutScatter.getInstance(this).parse(ResourceTable.Layout_common_layout, null, false);
        super.setUIContent(rootView);
        customHelper = CustomHelper.of(rootView, getAbility());
        initLayoutWidget();
    }

    /**
     * This method is used to initialize child component
     */
    private void initLayoutWidget() {
        Button btnTakePhoto = (Button) findComponentById(ResourceTable.Id_btnPickByTake);
        btnTakePhoto.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                onClickButton(component);
            }
        });
        Button btnSelectPhoto = (Button) findComponentById(ResourceTable.Id_btnPickBySelect);
        btnSelectPhoto.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                onClickButton(component);
            }
        });
    }

    /**
     * This method is used to handle click event on a button
     *
     * @param component Clicked component button
     */
    private void onClickButton(Component component) {
        if (null != mSimpleAbility) {
            customHelper.onClick(component, mSimpleAbility.getTakePhoto());
        }
    }
}
