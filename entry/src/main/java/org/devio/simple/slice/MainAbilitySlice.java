/*
 * Copyright (c) 2020 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.devio.simple.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import org.devio.simple.SimpleAbility;
import org.devio.simple.SimpleFractionAbility;
import org.devio.takephoto.ResourceTable;
import org.devio.takephoto.uitl.TConstant;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        ComponentContainer rootView = (ComponentContainer) LayoutScatter.getInstance(this).parse(ResourceTable.Layout_ability_main, null, false);
        initComponent(rootView);
        super.setUIContent(rootView);
    }

    /**
     * This method id used to initialize component of layout
     *
     * @param rootView parent view for getting child resources
     */
    private void initComponent(ComponentContainer rootView) {
        Button btnActivity = (Button) rootView.findComponentById(ResourceTable.Id_btnTakePhotoActivity);
        btnActivity.setText(btnActivity.getText().toUpperCase());
        btnActivity.setClickedListener(this);

        Button btnFragment = (Button) rootView.findComponentById(ResourceTable.Id_btnTakePhotoFragment);
        btnFragment.setText(btnFragment.getText().toUpperCase());
        btnFragment.setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btnTakePhotoActivity:
                launchAbility(SimpleAbility.class.getSimpleName());
                break;
            case ResourceTable.Id_btnTakePhotoFragment:
                launchAbility(SimpleFractionAbility.class.getSimpleName());
                break;
            default:
                break;
        }
    }

    /**
     * This method is used ro launch ability
     *
     * @param abilityName name of your ability class which you want to open
     */
    private void launchAbility(String abilityName) {
        Operation operationBuilder = new Intent.OperationBuilder()
                .withBundleName(TConstant.APP_BUNDLE_NAME)
                .withAbilityName(abilityName)
                .build();
        Intent intent = new Intent();
        intent.setOperation(operationBuilder);
        startAbility(intent);
    }
}
