## TakePhoto

## Introduction:
TakePhoto is an open-source tool library for obtaining photos (taking photos) and compressing images on Harmony devices.

## Usage Instructions:
1. Make the ability/fraction class which will extends the **TakePhotoAbility.java/TakePhotoFraction.java** class, these classes has configuration view with multiple UI fields
```
        public class SimpleAbility extends TakePhotoAbility {
	    @Override
	    protected void onStart(Intent intent) {
	        super.onStart(intent);
                setMainRoute(//SLICE_NAME);
	    }

	    @Override
	    protected void onAbilityResult(int requestCode, int resultCode, Intent resultData) {
	        super.onAbilityResult(requestCode, resultCode, resultData);
            }

            @Override
            public void takeCancel() {
                super.takeCancel();
            }

            @Override
            public void takeFail(TResult result, String msg) {
                super.takeFail(result, msg);
            }

            @Override
            public void takeSuccess(TResult result) {
                super.takeSuccess(result);
                // SHOW IMAGE
            }
	}
```
2. Change the configuration based on the fields like compression, size, pixel, etc & click on “Photographing” button to start camera.

3. Give the permissions of camera & storage & your camera interface will be ready to take pictures.

4. Click on “Take Photo” to take picture & show the results & “Switch Camera” for rotate camera front/back.

5. After taking the picture it will be compressed based on the configurations & show the results.

## Installation tutorial:
```
1. entry build.gradle:
Method 1:
    Generate the .har package through the library and add the .har package to the libs folder.
    Add the following code to the entry gradle:
        implementation fileTree  (dir: 'libs', include: ['*.jar', '*.har'])
        implementation 'io.openharmony.tpc.thirdlib:glide:1.0.4'

Method 2:
    allprojects{
        repositories{
            mavenCentral()
        }
    }
        implementation 'io.openharmony.tpc.thirdlib:TakePhoto:1.0.0'
        implementation 'io.openharmony.tpc.thirdlib:glide:1.0.4'
2. takephoto_library build.gradle:
Method 1:
    allprojects{
        repositories{
            mavenCentral()
        }
    }
        implementation 'io.openharmony.tpc.thirdlib:luban:1.0.0'
```

## License

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.